//
//  PhotosHandler.swift
//  FotoSort
//
//  Created by Markus Stöbe on 23.05.15.
//  Copyright (c) 2015 Markus Stöbe. All rights reserved.
//

import Foundation
import Photos

protocol photosHandlerDelegateProtocol {
    func photoHandlerIsReady(handler:PhotosHandler, loadedImage newImage:UIImage)
}

class PhotosHandler: NSObject, PHPhotoLibraryChangeObserver {
    
    //******************************************************************************************************************
    //MARK: - Properties and Outlets
    //******************************************************************************************************************
    var cameraRoll :PHFetchResult?
    var numberOfAvailableImages = 0
    var currentlyShowingImage   = 0
    var delegate :photosHandlerDelegateProtocol?
    
    let adjustmentFormatIdentifier = "de.macundi.fotoSort"      //let other Apps know who made the changes to this foto
    let adjustmentFormatVersion    = "1.0"
    
    //******************************************************************************************************************
    //MARK: - Lifecylce
    //******************************************************************************************************************
    override init() {
        super.init()

        //register for changes from other users/apps
        PHPhotoLibrary.sharedPhotoLibrary().registerChangeObserver(self)        
    }

    deinit {
        //deregister for changes
        PHPhotoLibrary.sharedPhotoLibrary().unregisterChangeObserver(self)
    }
    
    //******************************************************************************************************************
    //MARK: - Methods
    //******************************************************************************************************************
    func fetchCameraRoll(){
        //load some assets
        cameraRoll = PHAssetCollection.fetchAssetCollectionsWithType(PHAssetCollectionType.SmartAlbum,
                                                             subtype:PHAssetCollectionSubtype.SmartAlbumUserLibrary,
                                                             options:nil)
        
        //see if we got any entries
        if (cameraRoll?.count > 0) {
            print("We got something! Fetch returned \(cameraRoll!.count) images", terminator: "")
            numberOfAvailableImages = cameraRoll!.count;

            if cameraRoll!.firstObject is PHAssetCollection {
                //get first image from AssetCollection
                cameraRoll     = PHAsset.fetchAssetsInAssetCollection(cameraRoll!.firstObject as! PHAssetCollection, options: nil)
                let firstImage = cameraRoll!.firstObject as! PHAsset
                numberOfAvailableImages = cameraRoll!.count
                currentlyShowingImage   = 0;
                
                //finally load the image
                retrieveFotoForAsset(firstImage)
            }
       }
    }
    
    func fetchNextImage(){
        currentlyShowingImage++
        
        if currentlyShowingImage+1 > cameraRoll!.count {
            currentlyShowingImage = 0
        }
        
        let nextImage = cameraRoll![currentlyShowingImage] as! PHAsset
        retrieveFotoForAsset(nextImage)
    }
    
    func retrieveFotoForAsset(assetToLoad:PHAsset) {
        PHImageManager.defaultManager().requestImageForAsset(assetToLoad, targetSize:CGSize(width:assetToLoad.pixelWidth, height:assetToLoad.pixelHeight), contentMode:PHImageContentMode.AspectFit, options:nil, resultHandler:{ (nextImage, info) -> Void in
            self.delegate?.photoHandlerIsReady(self, loadedImage:nextImage!)
        })

    }
    
    func deleteCurrentImage() {
        //get Asset for currently showing image
        let currentImage = cameraRoll![currentlyShowingImage] as! PHAsset
        
        //delete it from Photo-Lib
        PHPhotoLibrary.sharedPhotoLibrary().performChanges({PHAssetChangeRequest.deleteAssets([currentImage])},
        completionHandler: {success, error in
            if success {
                self.fetchNextImage()
            } else {
                print("Finished deleting foto with error: \(error!.description)")
            }
        })
    }
    
    func changeCurrentImageToBW() {
        self.applyFilterNamed("CIPhotoEffectTonal")
    }

    func changeCurrentImageToSmooth() {
        self.applyFilterNamed("CIGaussianBlur")
    }
    
    func applyFilterNamed(filterName:String) {
        //create options-object
        var options = PHContentEditingInputRequestOptions()
        
        //make sure we get adjustement-data only if it'S our own adjustement, otherwise go get the modified image
        options.canHandleAdjustmentData = {
            (adjustmentData:PHAdjustmentData) in
            if (adjustmentData.formatIdentifier == self.adjustmentFormatIdentifier
            && adjustmentData.formatVersion     == self.adjustmentFormatVersion) {
                return true
            } else {
                return false
            }
        }
        
        //get Asset for currently showing image
        var currentImage = cameraRoll![currentlyShowingImage] as! PHAsset

        currentImage.requestContentEditingInputWithOptions(options, completionHandler: { (imageInput:PHContentEditingInput?,_: [NSObject : AnyObject]) -> Void in
            //get the image
            var imageURL = imageInput!.fullSizeImageURL
            var image    = CIImage(contentsOfURL: imageURL!)
            
            //apply orientation
            let imageOrient = imageInput!.fullSizeImageOrientation
            image           = image!.imageByApplyingOrientation(imageOrient)
    
            //now apply filter-effect with default-settings to our image
            let filter = CIFilter(name: filterName)
            filter!.setDefaults()
            filter!.setValue(image, forKey: kCIInputImageKey)
            
            //in order to save the image, we need to make an output-object first
            let output = PHContentEditingOutput(contentEditingInput: imageInput!)

            //let other apps know who made the changes
            output.adjustmentData = PHAdjustmentData(
                formatIdentifier:self.adjustmentFormatIdentifier,
                formatVersion:self.adjustmentFormatVersion,
                data:filterName.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!
            )

            //then we create a data-represantation of our modified image (for this we use CG/UIImage to use its convinence-converter to jpeg)
            let context       = CIContext(options:nil)
            let cgimg         = context.createCGImage(filter!.outputImage!, fromRect: filter!.outputImage!.extent)
            let jpegImageData = UIImageJPEGRepresentation(UIImage(CGImage: cgimg), 1.0)
            
            //and tell it where it should be stored
            jpegImageData!.writeToURL(output.renderedContentURL, atomically: true)

            //now let iOS take over and actually perform these changes on "disk"
            PHPhotoLibrary.sharedPhotoLibrary().performChanges(
                {
                    let changeRequest = PHAssetChangeRequest(forAsset: currentImage)
                    changeRequest.contentEditingOutput = output
                },
                completionHandler: {
                    (success, error) in
                        if success{
                            print("Successfully edited the image")
                            //at the end, reload the altered image
                            self.retrieveFotoForAsset(currentImage)
                        } else {
                            print("Couldn't edit the image. Error = \(error)")
                        }
                }
            )
        })
    }


    func revertToOriginalImage() {
        //get Asset for currently showing image
        let currentImage = cameraRoll![currentlyShowingImage] as! PHAsset
        let request = PHAssetChangeRequest(forAsset:currentImage)

        
        PHPhotoLibrary.sharedPhotoLibrary().performChanges(
            {
                request.revertAssetContentToOriginal()
            },
            completionHandler: {success, error in
                if success {
                    print("Foto is back to normal.")
                } else {
                    print("Finished processing foto with error: \(error!.description)")
                }
        })

    }
    
    func toggleFavoriteForAsset(asset: PHAsset) {
        PHPhotoLibrary.sharedPhotoLibrary().performChanges({
            // Create a change request from the asset to be modified.
            let request = PHAssetChangeRequest(forAsset: asset)
            
            // Set a property of the request to change the asset itself.
            request.favorite = !asset.favorite
            
            }, completionHandler: { success, error in
                NSLog("Finished updating asset. %@", (success ? "Success." : error!))
        })
    }
    
    //******************************************************************************************************************
    //MARK: - PHPhotoLibraryChangeObserver
    //******************************************************************************************************************
    func photoLibraryDidChange(changeInstance: PHChange) {
            print("behold…it changed! \(changeInstance.description)")
    }
    
    
    
}