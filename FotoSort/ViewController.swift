//
//  ViewController.swift
//  FotoSort
//
//  Created by Markus Stöbe on 22.05.15.
//  Copyright (c) 2015 Markus Stöbe. All rights reserved.
//

import UIKit
import Photos

class ViewController: UIViewController, photosHandlerDelegateProtocol {

    //******************************************************************************************************************
    //MARK: - Properties and Outlets
    //******************************************************************************************************************
    var photosHandler : PhotosHandler?
    @IBOutlet weak var imageView: UIImageView!
    
    //******************************************************************************************************************
    //MARK: - Lifecylce
    //******************************************************************************************************************
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //create a photohandler
        photosHandler = PhotosHandler.init()
        photosHandler!.delegate = self
    }
    
    override func viewDidAppear(animated: Bool) {
        photosHandler!.fetchCameraRoll()
    }

    //******************************************************************************************************************
    //MARK: - IBActions
    //******************************************************************************************************************
    @IBAction func processImageAndSwitchToNext(sender: UIButton) {
        switch sender.titleLabel!.text! {
        case "Behalten":
            print("Keep")
            photosHandler!.fetchNextImage()
        case "Löschen":
            print("Delete")
            photosHandler!.deleteCurrentImage()
        case "S/W":
            print("S/W")
            photosHandler!.changeCurrentImageToBW()
        case "Weich":
            print("Weichzeichner")
            photosHandler!.changeCurrentImageToSmooth()
        default:
            print("not yet implemented")
        }
        
    }
    
    //******************************************************************************************************************
    //MARK: - photosHandlerDelegateProtocol
    //******************************************************************************************************************
    func photoHandlerIsReady(handler:PhotosHandler, loadedImage newImage:UIImage) {
        print("\ngot a message from photohandler, she got an image of size \(newImage.size)")
        //now that a new image was loaded, show it in the UI
        self.imageView.image = newImage
    }


    
    
    


}

